import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { NativeStorage } from '@ionic-native/native-storage';

const SERVER_URL = 'http://api-vanhack-event-sp.azurewebsites.net/api/v1/';

const headers = new Headers({
  'Content-Type': 'application/json'
});
const options = new RequestOptions({
  headers: headers
});

@Injectable()
export class RestProvider {

	constructor(private http: Http,private nativeStorage: NativeStorage) {
	}

  public post(path:string,body:any) {
    return this.http.post(SERVER_URL + path, body, options).map(res => res);
	}

  public postLogin(body:any) {
    return this.http.post(SERVER_URL + "Customer/auth?" + "email="+body.email+"&password="+body.password, options).map((res:any) => res);
	}

  public get(path:string) {
    return this.http.get(SERVER_URL + path, options).map((data: any) => data);
	}

  public getOrders(path:string, optionsNew) {
    console.log(optionsNew)
     return this.http.get(SERVER_URL + path, optionsNew).map((data: any) => data);
	}



  public getStore(path:string,body:any) {
    return this.http.get(SERVER_URL + path + body, options)
    .map((data: any) => data);
	}

}
