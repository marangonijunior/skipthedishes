import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NativeStorage } from '@ionic-native/native-storage';

import { Skip } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { OrderPage } from '../pages/order/order';
import { CartPage } from '../pages/cart/cart';
import { SignPage } from '../pages/sign/sign';
import { ProductPage } from '../pages/products/products';
import { CousinePage } from '../pages/cousine/cousine';
import { CousineStorePage } from '../pages/cousineStore/cousineStore';

import { RestProvider } from '../provider/rest-provider';


@NgModule({
  declarations: [
    Skip,
    HomePage,
    CartPage,
    LoginPage,
    OrderPage,
    SignPage,
    ProductPage,
    CousinePage,
    CousineStorePage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(Skip)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Skip,
    HomePage,
    CartPage,
    LoginPage,
    OrderPage,
    SignPage,
    ProductPage,
    CousinePage,
    CousineStorePage
  ],
  providers: [
    RestProvider,
    NativeStorage,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
