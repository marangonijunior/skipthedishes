import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, Events  } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { OrderPage } from '../pages/order/order';
import { CartPage } from '../pages/cart/cart';
import { CousinePage } from '../pages/cousine/cousine';

@Component({
  templateUrl: 'app.html'
})
export class Skip {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = LoginPage;
  public pages: Array<{title: string, component: any, icon: any}>;

  constructor(
    public menu: MenuController,
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private nativeStorage: NativeStorage,
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      this.nativeStorage.setItem("cart", [])

      //Set Pages
      this.pages = [
        { title: 'Home', component: HomePage, icon: "home"},
        { title: 'Cousine', component: CousinePage, icon: "pizza" },
        { title: 'Orders', component: OrderPage, icon: "bookmarks" },
        { title: 'Cart', component: CartPage, icon: "cart" }
      ];

    });
  }

  openPage(page) {
    this.menu.close();
    this.nav.setRoot(page.component);
  }

}
