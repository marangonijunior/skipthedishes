import { Component } from '@angular/core';
import { NavController, MenuController, Nav, ToastController,Platform } from 'ionic-angular';

import { CartPage } from '../../pages/cart/cart';
import { ProductPage } from '../../pages/products/products';
import { RestProvider } from '../../provider/rest-provider';
import { NativeStorage } from '@ionic-native/native-storage';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  cart = CartPage;
  loading:any;
  listStore:Array<any> = [];
  search:any;
  constructor(
    public navCtrl: NavController,
    public nav: Nav,
    public menuCtrl: MenuController,
    public toast: ToastController,
    public server: RestProvider,
    public platform: Platform
  ) {
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);
  }

  ionViewDidLoad() {
    this.getStores();
   }

   ionViewDidEnter(){

   }

   getFilter(e){

     if(this.search.split('').length > 1){
       this.server.getStore("Store/search/",this.search).subscribe(
         result => {
           this.loading = false;
           let data = JSON.parse(result._body);
           this.listStore = data;
         },
         err => {
           this.loading = false;
           if(err._body){
             let msg = JSON.parse(err._body);
             this.toastUser(msg.error)
           }else{
             this.toastUser("Ops try again")
           }
         }
       );
     }
     if(this.search.split('').length == 0){
       this.getStores()
     }

   }

   getStores(){
     this.server.get("Store").subscribe(
       result => {
         this.loading = false;
         let data = JSON.parse(result._body);
         this.listStore = data;
       },
       err => {
         this.loading = false;
         if(err._body){
           let msg = JSON.parse(err._body);
           this.toastUser(msg.error)
         }else{
           this.toastUser("Ops try again")
         }
       }
     );
   }

    select(item){
      this.nav.push(ProductPage, item)
    }

    toastUser(msg:string){
        let alertToast = this.toast.create({
          message: msg,
          duration: 1000,
          position: 'top'
        });
        alertToast.present()
    }

}
