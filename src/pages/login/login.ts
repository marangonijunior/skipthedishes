import { Component } from '@angular/core';
import { Events, ToastController, Nav, NavController, MenuController, Platform  } from 'ionic-angular';

import { HomePage } from '../../pages/home/home';
import { SignPage } from '../../pages/sign/sign';

import { NativeStorage } from '@ionic-native/native-storage';
import { RestProvider } from '../../provider/rest-provider';


@Component({
  templateUrl: 'login.html'
})
export class LoginPage {

  signpage = SignPage;
  loading = false;

  email:any;
  password:any;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toast: ToastController,
    public platform: Platform,
    private events: Events,
    public server: RestProvider,
    private nativeStorage: NativeStorage)
    {}


  ionViewDidLoad() {
    this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
    //this.navCtrl.setRoot(HomePage)
 }

 ionViewDidEnter(){

 }

 login(){

     if(
       this.email &&
       this.password
     ){
       let body = {
         "email": this.email,
         "password": this.password
       };
       this.loading = true;
       this.server.postLogin(body).subscribe(
         result => {
           this.loading = false;
           this.nativeStorage.setItem("token", result._body)
           this.navCtrl.setRoot(HomePage)
         },
         err => {
           this.loading = false;
           if(err._body){
             let msg = JSON.parse(err._body);
             this.toastUser(msg.error)
           }else{
             this.toastUser("Ops try again")
           }
         }
       );
     }else{
       this.toastUser("You need insert all data. ")
     }
 }

  toastUser(msg:string){
      let alertToast = this.toast.create({
        message: msg,
        duration: 1000,
        position: 'top'
      });
      alertToast.present()
  }

}
