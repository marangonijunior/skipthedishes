import { Component } from '@angular/core';
import { Events, ToastController, NavController, MenuController, Platform  } from 'ionic-angular';
import {Http, Headers, RequestOptions} from '@angular/http';

import { NativeStorage } from '@ionic-native/native-storage';
import { RestProvider } from '../../provider/rest-provider';


@Component({
  templateUrl: 'order.html'
})
export class OrderPage {

  listOrders:Array<any> = [];
  loading = false;
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toast: ToastController,
    public server: RestProvider,
    private nativeStorage: NativeStorage)
    {
      this.menuCtrl.enable(true);
      this.menuCtrl.swipeEnable(true);
    }


  ionViewDidLoad() {
    this.getAll();
  }

  ionViewDidEnter(){
  }

  getAll(){

    let headersNew;
    let optionsNew;
    this.nativeStorage.getItem('token')
     .then(
     data => {
         if(data){
           headersNew = new Headers({
             'Authorization': "Bearer "+ data,
             'Content-Type': 'application/json'
           });
           optionsNew = new RequestOptions({
             headers: headersNew
           });
           this.server.getOrders("Order/customer", optionsNew).subscribe(
             result => {
               this.loading = false;
               let data = JSON.parse(result._body);
               this.listOrders = data;
             },
             err => {
               this.loading = false;
               if(err._body){
                 let msg = JSON.parse(err._body);
                 this.toastUser(msg.error)
               }else{
                 this.toastUser("Ops try again")
               }
             }
           );
         }
     });


  }

  toastUser(msg:string){
      let alertToast = this.toast.create({
        message: msg,
        duration: 1000,
        position: 'top'
      });
      alertToast.present()
  }

}
