import { Component } from '@angular/core';
import { Events, AlertController, ToastController, NavController, MenuController, Platform  } from 'ionic-angular';

import { NativeStorage } from '@ionic-native/native-storage';
import { RestProvider } from '../../provider/rest-provider';
import { OrderPage } from '../../pages/order/order';


@Component({
  templateUrl: 'cart.html'
})
export class CartPage {

  listCard:Array<any> = [];
  value:any = 0;

  constructor(
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toast: ToastController,
    public server: RestProvider,
    private nativeStorage: NativeStorage)
    {}


  ionViewDidLoad() {
    this.listCard = [{id: 1, storeId: 1, name: "Shrimp Tempura", description: "Fresh shrimp battered and deep fried until golden brown", price: 10.95},
{id: 2, storeId: 1, name: "Shrimp with Snow Peas and Cashew", description: "A delicious combination of fresh shrimp, snow peas, and cashew", price: 12.5},
{id: 3, storeId: 1, name: "Special Deep-Fried Fish", description: "Tilapia fish deep fried until flaky and tender", price: 12.95}];
    this.getValue();
    this.nativeStorage.getItem('cart')
     .then(
     data => {
         if(data){
           this.listCard = data;
           this.getValue();
         }
     });
  }

  ionViewDidEnter(){
  }

  delete(item){

    for (var i = 0; i < this.listCard.length; i++) {
      if(item.id == this.listCard[i].id){
        this.listCard.splice(i,1);
        this.nativeStorage.setItem('cart', this.listCard);
        this.getValue();
        this.toastUser("Product removed!")
      }
    }

  }

  getValue(){
    this.value = 0;
    for (var i = 0; i < this.listCard.length; i++) {
      this.value = this.value + this.listCard[i].price;
    }
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm purchase',
      message: 'Do you want to buy this products?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Buy',
          handler: () => {
            this.order();
          }
        }
      ]
    });
    alert.present();
  }

  order(){

  }

  toastUser(msg:string){
      let alertToast = this.toast.create({
        message: msg,
        duration: 1000,
        position: 'top'
      });
      alertToast.present()
  }

}
