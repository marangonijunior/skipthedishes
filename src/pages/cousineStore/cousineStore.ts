import { Component } from '@angular/core';
import { Events, Nav, NavParams, ToastController, NavController, MenuController, Platform  } from 'ionic-angular';

import { NativeStorage } from '@ionic-native/native-storage';
import { RestProvider } from '../../provider/rest-provider';
import { CartPage } from '../../pages/cart/cart';
import { ProductPage } from '../../pages/products/products';


@Component({
  templateUrl: 'cousineStore.html'
})
export class CousineStorePage {
  cart = CartPage;
  search:any;
  loading = false;
  listStore:Array<any> = [];
  cousineStore:any;
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toast: ToastController,
    public server: RestProvider,
    public navParams: NavParams,
    public nav: Nav,
    private nativeStorage: NativeStorage)
    {
      this.menuCtrl.enable(true);
      this.menuCtrl.swipeEnable(true);
      this.cousineStore = this.navParams.data;
    }


  ionViewDidLoad() {
    this.getStores();
  }

  ionViewDidEnter(){
  }

  getStores(){
    this.server.get("Cousine/" + this.cousineStore.id + "/stores").subscribe(
      result => {
        this.loading = false;
        let data = JSON.parse(result._body);
        this.listStore = data;
      },
      err => {
        this.loading = false;
        if(err._body){
          let msg = JSON.parse(err._body);
          this.toastUser(msg.error)
        }else{
          this.toastUser("Ops try again")
        }
      }
    );
  }

 select(item){
   this.nav.push(ProductPage, item)
 }

  toastUser(msg:string){
      let alertToast = this.toast.create({
        message: msg,
        duration: 1000,
        position: 'top'
      });
      alertToast.present()
  }

}
