import { Component } from '@angular/core';
import { Events, NavParams, ToastController, NavController, MenuController, Platform  } from 'ionic-angular';

import { NativeStorage } from '@ionic-native/native-storage';
import { RestProvider } from '../../provider/rest-provider';
import { CartPage } from '../../pages/cart/cart';


@Component({
  templateUrl: 'products.html'
})
export class ProductPage {
  cart = CartPage;
  search:any;
  loading = false;
  listProducts:Array<any> = [];
  listCard:Array<any> = [];
  store:any;
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toast: ToastController,
    public server: RestProvider,
    public navParams: NavParams,
    private nativeStorage: NativeStorage)
    {
      this.menuCtrl.enable(true);
      this.menuCtrl.swipeEnable(true);
      this.store = this.navParams.data;
    }


  ionViewDidLoad() {
    this.nativeStorage.setItem('cart', [])
    this.getAll();
  }

  ionViewDidEnter(){
  }

  selectProduct(i){
    this.nativeStorage.getItem('cart')
     .then(
     data => {
         if(data){
           this.listCard = data;
           this.listCard.push(i);
           this.nativeStorage.setItem('cart', this.listCard)
           this.toastUser("Product added in cart")
         }
     });
  }

  getAll(){
    this.server.get("Store/" + this.store.id + "/products").subscribe(
      result => {
        this.loading = false;
        let data = JSON.parse(result._body);
        this.listProducts = data;
      },
      err => {
        this.loading = false;
        if(err._body){
          let msg = JSON.parse(err._body);
          this.toastUser(msg.error)
        }else{
          this.toastUser("Ops try again")
        }
      }
    );
  }

  toastUser(msg:string){
      let alertToast = this.toast.create({
        message: msg,
        duration: 1000,
        position: 'top'
      });
      alertToast.present()
  }

}
