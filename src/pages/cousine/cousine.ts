import { Component } from '@angular/core';
import { NavController, MenuController, Nav, ToastController,Platform } from 'ionic-angular';

import { CartPage } from '../../pages/cart/cart';
import { CousineStorePage } from '../../pages/cousineStore/cousineStore';
import { RestProvider } from '../../provider/rest-provider';
import { NativeStorage } from '@ionic-native/native-storage';


@Component({
  selector: 'cousine',
  templateUrl: 'cousine.html'
})
export class CousinePage {
  cart = CartPage;
  loading:any;
  listCousine:Array<any> = [];
  search:any;
  constructor(
    public navCtrl: NavController,
    public nav: Nav,
    public menuCtrl: MenuController,
    public toast: ToastController,
    public server: RestProvider,
    public platform: Platform
  ) {
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeEnable(true);
  }

  ionViewDidLoad() {
    this.getCousines();
   }

   ionViewDidEnter(){

   }

   getFilter(e){

     if(this.search.split('').length > 1){
       this.server.getStore("Cousine/search/",this.search).subscribe(
         result => {
           this.loading = false;
           let data = JSON.parse(result._body);
           this.listCousine = data;
         },
         err => {
           this.loading = false;
           if(err._body){
             let msg = JSON.parse(err._body);
             this.toastUser(msg.error)
           }else{
             this.toastUser("Ops try again")
           }
         }
       );
     }
     if(this.search.split('').length == 0){
       this.getCousines()
     }

   }

   getCousines(){
     this.server.get("Cousine").subscribe(
       result => {
         this.loading = false;
         let data = JSON.parse(result._body);
         this.listCousine = data;
       },
       err => {
         this.loading = false;
         if(err._body){
           let msg = JSON.parse(err._body);
           this.toastUser(msg.error)
         }else{
           this.toastUser("Ops try again")
         }
       }
     );
   }

    select(item){
      this.nav.push(CousineStorePage, item)
    }

    toastUser(msg:string){
        let alertToast = this.toast.create({
          message: msg,
          duration: 1000,
          position: 'top'
        });
        alertToast.present()
    }

}
