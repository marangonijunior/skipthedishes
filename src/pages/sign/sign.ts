import { Component } from '@angular/core';
import { Events, ToastController, NavController, MenuController, Platform  } from 'ionic-angular';

import { NativeStorage } from '@ionic-native/native-storage';
import { RestProvider } from '../../provider/rest-provider';


@Component({
  templateUrl: 'sign.html'
})
export class SignPage {
  loading = false;

  email:any;
  name:any;
  address:any;
  password:any;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toast: ToastController,
    public server: RestProvider,
    private nativeStorage: NativeStorage)
    {}


  ionViewDidLoad() {
  }

  ionViewDidEnter(){
  }

  signUser(){

    if(
      this.email &&
      this.name &&
      this.address &&
      this.password
    ){
      let body = {
        "id":20181102,
        "email": this.email,
        "name": this.name,
        "address": this.address,
        "creation": "2018-03-18T14:36:13.216Z",
        "password": this.password
      };
      this.loading = true;
      this.server.post("Customer", body).subscribe(
        result => {
          this.loading = false;
          this.toastUser("Thanks, please make login now.")
          this.navCtrl.pop()
        },
        err => {
          this.loading = false;
          if(err._body){
            let msg = JSON.parse(err._body);
            this.toastUser(msg.error)
          }else{
            this.toastUser("Ops try again")
          }

        }
      );
    }else{
      this.toastUser("You need insert all data. ")
    }
  }

  toastUser(msg:string){
      let alertToast = this.toast.create({
        message: msg,
        duration: 1000,
        position: 'top'
      });
      alertToast.present()
  }

}
